import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { List } from '../lists';
import { herolist } from '../list-heroes';


@Injectable({
  providedIn: 'root',
})

export class HeroService {

  constructor() { }

  getHeroes(): Observable<List[]> {
        this.messageService.add('HeroService: fetched heroes');
    return of(HEROES);
  }
}