import { Component, OnInit } from '@angular/core';
import { List } from '../lists';
import { herolist } from '../list-heroes';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})

export class ModalComponent implements OnInit {
 
  constructor() { }

  ngOnInit(): void { }

}
