import { ComponentFixture, TestBed } from '@angular/core/testing';
import { List } from '../lists';
import { herolist } from '../list-heroes';
import { }


import { SrviceComponent } from './srvice.component';

@Component({
   selector: 'app-srvice',
  templateUrl: './srvice.component.html',
  styleUrls: ['./srvice.component.css']
})

getHeroes(): List[]{
  return herolist;
}

@Injectable({
  providedIn: 'root',
})