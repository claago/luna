import { Component } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.css']
})

export class ModelsComponent{

  constructor(public dialog: MatDialog) { }

  openDialog(): void {
  	this.dialog.open(ModelsComponent);
  	//console.log(this.dialog);
  }
}

@Component({
   selector: 'app-models',
  templateUrl: './models.component.dialog.html',
})

export class ModelsComponentDialog {

}